curl -XPUT http://$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/${1} -d '{    
	"settings" : {
        	"number_of_shards" : 1
    	},
    	"mappings" : {
        	"twit" : {
            		"properties" : {
                		"screen_name" : { "type" : "string", "index" : "analyzed" },
				"text" : { "type" : "string", "index" : "not_analyzed" },
				"created_at" : { "type" : "string", "index" : "not_analyzed" },
				"retweet_count" : { "type" : "string", "index" : "not_analyzed" },
				"id" : { "type" : "string", "index" : "not_analyzed" },
				"coordinates" : { "type" : "string", "index" : "not_analyzed" }
           	 	}
        	}
    	}
}'
