curl -XGET http://$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/${1}/${1}/_search?pretty -d '{
	"query": {
        	"regexp": {
        		"'${2}'": {
				"value": "'${3}'" 
			}
		}
	}
}'
