curl -XGET http://$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/_cat/shards
curl -XGET http://$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/_cat/health
curl -XGET http://$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/_cat/indices?v
