echo $(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)
for shard in $(curl -XGET http://$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/_cat/shards | grep UNASSIGNED | awk '{print $2}'); do
curl -XPOST "$(egrep -o "search.*amazonaws.com" ../ews/conf/app_config.json | uniq)/_cluster/reroute" -d '{
        "commands" : [ {
              "allocate" : {
                  "index" : "twit", 
                  "shard" : $shard, 
                  "node" : "Ororo Munroe" 
              }
            }
        ]
    }'
    sleep 5
done
