#!/usr/bin/python
import sys
import os
script_path = os.path.split(os.path.realpath(__file__))[0].split('/')
sys.path.append('%s/mod/' % "/".join(script_path[:-2]))
import logging
from TwitterAPI import TwitterAPI
import json
import csv
import os.path
import fnmatch
from Connector import *
from Loader import *
from EWSLoader import EWSLoader
from types import ClassType
import cStringIO

COUNTRY='ly'

if __name__ == "__main__":
	my_twitter = None
	ewsloader = None
	aws = None	# placeholder for amazon elastic search connector
	
	logging.basicConfig(level=logging.DEBUG,
			filename='/tmp/twit.txt',
			format='%(asctime)s, %(message)s')
        logging.info('STARTED')

	try:
		ewsloader = EWSLoader('app_config.json','config.json',"/".join(script_path[:-2]))


       		# create AWS connector
		try:
        		aws = AWSConnector(ewsloader.app_config_json['elasticsearch'],ewsloader.country_config_json["indice_mapping"])
			aws_response = json.loads(aws.curl_get(ewsloader.app_config_json['index']['twit']))
			if ewsloader.app_config_json['index']['twit'] in aws_response:
				logging.info("AWS, PASS, index %s exists %s" % (ewsloader.app_config_json['index']['twit'],aws_response))
			else:
				logging.debug("AWS, Error, index %s missing" % ewsloader.app_config_json['index']['twit'])
				# create index
				index_setting = {
        				"settings" : {
                				"number_of_shards" : 1
        				}
				}
                        	index_setting["mappings"] = {
					ewsloader.app_config_json['index']['twit'] : {
                                        	"properties" : {
                                        		"screen_name" : { "type" : "string", "index" : "analyzed" },
                                                	"text" : { "type" : "string", "index" : "not_analyzed" },
                                               		"created_at" : { "type" : "string", "index" : "not_analyzed" },
                                                	"retweet_count" : { "type" : "string", "index" : "not_analyzed" },
                                                	"id" : { "type" : "string", "index" : "not_analyzed" },
                                                	"coordinates" : { "type" : "string", "index" : "not_analyzed" }
                        			}
					}
				}

				aws_response = json.loads(aws.curl_put_index(ewsloader.app_config_json['index']['twit'],index_setting))
				if aws_response and 'acknowledged' in aws_response:
					if str(aws_response['acknowledged']) == "True":
						logging.info("AWS, PASS, index %s created %s" % (ewsloader.app_config_json['index']['twit'],aws_response))
					else:
						logging.debug("AWS, Error, unable to creaate index %s" % (ewsloader.app_config_json['index']['twit'],aws_response))
						exit(1)
		except Exception as ve:
			logging.debug("AWS, Exception, AWSConnector Exception : %s" % str(ve))
			exit(1)

        	country = Monitor(ewsloader.country_config_json[COUNTRY]['track'][0],
                	ewsloader.country_config_json[COUNTRY]['log_file'])
		logging.info("Country %s: PASS, Loaded %s" % (COUNTRY,ewsloader))
		try:
			my_twitter = TwitterConnector(ewsloader.app_config_json['credential']['CONSUMER_KEY'],
                        	ewsloader.app_config_json['credential']['CONSUMER_SECRET'],
                       	 	ewsloader.app_config_json['credential']['ACCESS_TOKEN_KEY'],
                        	ewsloader.app_config_json['credential']['ACCESS_TOKEN_SECRET'])
			logging.info("TWITTER, PASS, my_twitter created %s" % my_twitter)
		except Exception as e:
			logging.debug("TWITTER, Exception, TwitterConnector Exception caught: %s" % str(e))
			exit(1)
		if my_twitter:
			if my_twitter.validate_track_term(ewsloader.country_config_json[COUNTRY]['track']):
				track = []
				track = my_twitter.limit_track(ewsloader.country_config_json[COUNTRY]['track'])
				logging.info("TWITTER, PASS, track validated and length is %s" % len(track))
				if len(track) > 0:
					twitter_req = my_twitter.request('statuses/filter', {'track': track[0]})
					if twitter_req:
	                                        with open("%s/%s" % ("/".join(script_path[:-2]),ewsloader.country_config_json[COUNTRY]['log_file']), 'a') as log_twit_fd:
        	                                        writer = csv.DictWriter(
                	                                        log_twit_fd, fieldnames=ewsloader.app_config_json['log']['FIELDS'], delimiter=',', quoting=csv.QUOTE_ALL)
                        	                        logging.info('LOG_TWIT: Started')
							for twit in twitter_req:
                                                		if 'text' in twit:
									try:
										simple_twit = my_twitter.extract_fields(twit,COUNTRY)
										if simple_twit:
											aws_put_resp = aws.curl_put_twit(simple_twit)
											logging.info("AWS, PASS, put %s twit:%s" % (aws_put_resp, simple_twit["id"]))
											writer.writerow(simple_twit)
									except Exception as e:
	                        						logging.debug("AWS, Exception, put Exception : %s" % str(e))
                        							exit(1)								
			else:
				logging.info("TWITTER, FAILED, invalid track")
				exit(1)
				
	except Exception as ews:
		logging.debug("Country %s: Exception, when loading: %s" %(COUNTRY,str(ews)))
		exit(1)
		
