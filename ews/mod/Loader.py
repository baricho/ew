class Country(object):
	"""concept for country
	
	Attributes:
		track: string for twitter feed search
		log_file: string for log location
	"""

	def __init__(self, track, log_file):
        	"""Return a new country object."""
		self.track = track
		self.log_file = log_file

class Monitor(Country):
	def __init__(self, track, log_file):
		"""return ethiopia object"""
		self.log_file = log_file
		self.track = track
