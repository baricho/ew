import pycurl
import json
import cStringIO
import urllib
from TwitterAPI import TwitterAPI
from googleapiclient.discovery import build
import feedparser
import re

class RSSConnector(object):
	url = ""
	feed = None

	def __init__(self,url):
		self.url = url

	def parse_feed(self):
		self.feed = feedparser.parse(self.url)

	def get_feed(self,country_regex):
		#
		# figure out which posts to print
		#
		filtered_posts = []
		filter_regex = re.compile(country_regex,re.IGNORECASE)
		for post in self.feed.entries:
			m = filter_regex.search(post.title)		
			if m:
				filtered_posts.append(post)
		return filtered_posts

	def extract_fields(self,result):
                '''log every single rss search result, csv, comma separated'''
                result_hash = {
                        'published': "%s" % result['published'].encode("utf-8").replace("\n"," "),
                        'link': "%s" % result['link'].encode("utf-8").replace("\n"," "),
                        'summary': "%s" % result['summary'].encode("utf-8").replace("\n"," "),
                        'timestamp': "%s" % result['timestamp'],
			'country': "%s" % result['country']
                }
                return result_hash

class GoogleConnector(object):
	google_api = ""
	google_api_v = ""
	developerKey = ""
	cx = ""
	service = None

        def __init__(self,google_api,google_api_v,developerKey,cx):
        	self.google_api = google_api
        	self.google_api_v = google_api_v
        	self.developerKey = developerKey
        	self.cx = cx
		self.service = build(google_api,google_api_v,
            		developerKey=developerKey)

	''' Returns title and Snippet of google search item '''
	def return_website_name(self,google_item):
		website_name = ''
		if 'pagemap' in google_item:
                	if 'website' in google_item['pagemap']:
                        	if 'title' in google_item['pagemap']['website'][0]:
                                	website_name =  google_item['pagemap']['metatags']
        		if 'metatags' in google_item['pagemap']:
				if 'og:title' in google_item['pagemap']['metatags'][0]:
					website_name = google_item['pagemap']['metatags'][0]['og:title']
		if 'snippet' in google_item:
			website_name += ". %s" % google_item['snippet']
		return website_name

	def get_search(self,search_query):
		# Build a service object for interacting with the API. Visit
    		# the Google APIs Console <http://code.google.com/apis/console>
    		# to get an API key for your own application.
    		res = self.service.cse().list(
        		q=search_query,
        		cx=self.cx,
    		).execute()
    		return res

        def extract_fields(self,result):
                '''log every single google search result, csv, comma separated'''
                result_hash = {
                        'title': "%s" % result['title'].encode("utf-8").replace("\n"," "),
                        'snippet': "%s" % result['snippet'].encode("utf-8").replace("\n"," "),
                        'link': "%s" % result['link'].encode("utf-8").replace("\n"," "),
			'cacheId': "%s" % result['cacheId'].encode("utf-8").replace("\n"," "),
			'kind': "%s" % result['kind'].encode("utf-8").replace("\n"," "),
                        'timestamp': "%s" % result['timestamp'],
			'country': "%s" % result['country']
                }
                return result_hash


'''Class used to connect to TwitterAPI
Collects twits
'''
class TwitterConnector(object):
        consumer_key = ''
        consumer_secret = ''
        access_token_key = '' 
        access_token_secret = ''
        twitter_conn = None
	MAX_TRACK = 60

        def __init__(self,consumer_key,consumer_secret,access_token_key,access_token_secret):
                self.consumer_key = consumer_key
                self.consumer_secret = consumer_secret
                self.access_token_key = access_token_key 
                self.access_token_secret = access_token_secret
		try:
                	self.twitter_conn = self.connect()
		except:
			raise

        def connect(self):
                '''connect to Twitter using TwitterAPI'''
                try:
                        return TwitterAPI(self.consumer_key,
                                self.consumer_secret,
                                self.access_token_key,
                                self.access_token_secret)
                except:
                        raise

        def validate_track_term(self,track):
                '''check TRACK field is not empty'''
                return True if track != '' else False

        def request(self,twit_filter,json_request):
                '''connect to twitter and make the request'''
                req = None
                try:
                        req = self.twitter_conn.request(twit_filter,json_request)
			return req
                except:
                        raise

        def limit_track(self,tracks):
                limited_track = []
                build_track = ''
                for t in tracks:
                        if (len(bytes(build_track)) + len(bytes(t)) + len(bytes(','))) <= 60:
                                if build_track == '':
                                        build_track = t
                                else:
                                        build_track = '%s,%s' % (t,build_track)
                        else:
                                limited_track.append(build_track)
                                build_track = t
		limited_track.append(build_track)
                return limited_track

	def extract_fields(self,twit,country):
        	'''log every single twit, csv, comma separated'''
		loc = 'Unknown'
		if twit['user']['location']:
			eloc = twit['user']['location'].encode("utf-8").replace("\n"," ")
			loc = re.sub('\W+','_', eloc )

        	twit_hash = {
                	'created_at': "%s" % twit['created_at'],
                	'country': "%s" % country,
                	'id': "%s" % twit['id'],
                	'screen_name': "%s" % twit['user']['screen_name'],
			'location': "%s" % loc,
                	'coordinates': "%s" % twit['coordinates'],
                	'retweet_count': "%s" % twit['retweet_count'],
                	'text': "%s" % twit['text'].encode("utf-8").replace("\n"," ")
        	}
        	return twit_hash



"""AWS Elasticsearch Service connector object
uses pycurl to interact with service
"""
class AWSConnector(object):
        indice = ''
        mapping = ''
	endpoint = ''
	domainarn = ''
	kibana = ''

	def __init__(self,elastic,indice_mapping):
		if len(elastic) == 3 and len(indice_mapping) == 2:
			self.endpoint = elastic["endpoint"]
        		self.domainarn = elastic["domainarn"]
        		self.kibana = elastic["kibana"]
                	self.indice = indice_mapping[0]
               	 	self.mapping = indice_mapping[1]
		else:
			raise ValueError("Configuration is empty")

	def curl_put_twit(self,twit):
        	'''PUT twit log to AWS Elasticsearch endpoint
        	using pycurl'''
        	data = json.dumps(twit)
        	aws_url = 'https://' + self.endpoint +'/' + self.indice + '/' + self.mapping +'/' + twit['id']
        	if aws_url != '':
                	try:
                        	response = cStringIO.StringIO()
                        	c = pycurl.Curl()
                        	c.setopt(pycurl.URL, str(aws_url))
                        	c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json', 'Accept: application/json'])
                        	c.setopt(pycurl.CUSTOMREQUEST, "PUT")
                        	c.setopt(pycurl.POSTFIELDS,data)
                        	c.setopt(c.WRITEFUNCTION, response.write)
                        	c.perform()
                        	c.close()
				return response.getvalue()
                	except Exception as e:
				raise

        def curl_put_google(self,item):
                '''PUT google log to AWS Elasticsearch endpoint
                using pycurl'''
                data = json.dumps(item)
                aws_url = 'https://' + self.endpoint +'/' + self.indice + '/' + self.mapping +'/' + item['cacheId']
                if aws_url != '':
                        try:
                                response = cStringIO.StringIO()
                                c = pycurl.Curl()
                                c.setopt(pycurl.URL, str(aws_url))
                                c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json', 'Accept: application/json'])
                                c.setopt(pycurl.CUSTOMREQUEST, "PUT")
                                c.setopt(pycurl.POSTFIELDS,data)
                                c.setopt(c.WRITEFUNCTION, response.write)
                                c.perform()
                                c.close()
                                return response.getvalue()
                        except Exception as e:
                                raise

        def curl_put_generic(self,item,key):
                '''PUT to AWS Elasticsearch endpoint
                using pycurl'''
                data = json.dumps(item)
                aws_url = 'https://' + self.endpoint +'/' + self.indice + '/' + self.mapping +'/' + key
                if aws_url != '':
                        try:
                                response = cStringIO.StringIO()
                                c = pycurl.Curl()
                                c.setopt(pycurl.URL, str(aws_url))
                                c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json', 'Accept: application/json'])
                                c.setopt(pycurl.CUSTOMREQUEST, "PUT")
                                c.setopt(pycurl.POSTFIELDS,data)
                                c.setopt(c.WRITEFUNCTION, response.write)
                                c.perform()
                                c.close()
                                return response.getvalue()
                        except Exception as e:
                                raise

	def curl_put(self,item):
		'''PUT item to aws
		'''
                aws_url = 'https://' + self.endpoint +'/' + item + '?pretty'
                if aws_url != '' and item != '':
                        try:
                                response = cStringIO.StringIO()
                                c = pycurl.Curl()
                                c.setopt(pycurl.URL, str(aws_url))
                                c.setopt(pycurl.CUSTOMREQUEST, "PUT")
                                c.setopt(c.WRITEFUNCTION, response.write)
                                c.perform()
                                c.close()
				return response.getvalue()
                        except Exception as e:
                                raise

        def curl_put_index(self,item,index_setting):
                '''PUT index to aws
                '''
		data = json.dumps(index_setting)
                aws_url = 'https://' + self.endpoint +'/' + item
                if aws_url != '' and item != '':
                        try:
                                response = cStringIO.StringIO()
                                c = pycurl.Curl()
                                c.setopt(pycurl.URL, str(aws_url))
                                c.setopt(pycurl.CUSTOMREQUEST, "PUT")
				c.setopt(pycurl.POSTFIELDS,data)
                                c.setopt(c.WRITEFUNCTION, response.write)
                                c.perform()
                                c.close()
                                return response.getvalue()
                        except Exception as e:
                                raise

        def curl_get(self,item):
                '''get elastic _cat info
                '''
                aws_url = 'https://' + self.endpoint +'/' + item + '?pretty'
                if aws_url != '' and item != '':
                        try:
                                response = cStringIO.StringIO()
                                c = pycurl.Curl()
                                c.setopt(pycurl.URL, str(aws_url))
                                c.setopt(pycurl.CUSTOMREQUEST, "GET")
                                c.setopt(c.WRITEFUNCTION, response.write)
                                c.perform()
                                c.close()
				return response.getvalue()
                        except Exception as e:
                                raise
