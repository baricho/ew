import os.path
import json
import fnmatch

'''Loads country configuration
'''
class EWSLoader(object):
    home_dir = '.'
    country_config_json = None
    app_config_json = None

    def __init__(self,app_pattern,country_pattern,home_dir):
        if os.path.exists(home_dir):
            	self.home_dir = home_dir
            	config = self.locate(pattern=app_pattern,root=self.home_dir)
		try:
            		self.app_config_json = self.load_json(config)
            		config = self.locate(pattern=country_pattern,root=self.home_dir)
            		self.country_config_json = self.load_json(config)
		except:
			raise

    def locate(self,pattern, pathpattern="*", root=os.curdir):
        '''Locate all files matching supplied filename pattern in and below
        supplied root directory.'''
        for path, dirs, files in os.walk(os.path.abspath(root)):
            if fnmatch.fnmatch(path, pathpattern):
                for filename in fnmatch.filter(files, pattern):
                    return os.path.join(path, filename)

    def load_json(self,json_file):
        '''load json data from config file'''
	try:
        	if os.path.exists(json_file) and os.access(json_file, os.R_OK):
                	return json.loads(open(json_file).read())
        	else:
                	return None
	except:
		raise
