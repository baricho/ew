#!/usr/bin/python
import sys
import os
script_path = os.path.split(os.path.realpath(__file__))[0].split('/')
sys.path.append('%s/mod/' % "/".join(script_path[:-2]))
import logging
import json
from Connector import *
from Loader import *
from EWSLoader import EWSLoader
import time
import csv

COUNTRY='ss'

if __name__ == "__main__":
    ewsloader = None
    aws = None      # placeholder for amazon elastic search connector

    ''' TEMPLATE START '''
    logging.basicConfig(level=logging.DEBUG,
        filename='/tmp/rss.txt',
        format='%(asctime)s, %(message)s')
    logging.info('STARTED')

    try:
        ewsloader = EWSLoader('app_config.json','config.json',"/".join(script_path[:-2]))

        # create AWS connector
        try:
            aws = AWSConnector(ewsloader.app_config_json['elasticsearch'],ewsloader.country_config_json["rss_indice_mapping"])
            aws_response = json.loads(aws.curl_get(ewsloader.app_config_json['index']['rss']))
            if ewsloader.app_config_json['index']['rss'] in aws_response:
                logging.info("AWS, PASS, index %s exists %s" % (ewsloader.app_config_json['index']['rss'],aws_response))
            else:
                logging.debug("AWS, Error, index %s missing" % ewsloader.app_config_json['index']['rss'])
                # create index

                index_setting = {
                        "settings" : {
                                "number_of_shards" : 1
                        }
		}
		index_setting["mappings"] = {
			ewsloader.app_config_json['index']['rss'] : {
                		"properties" : {
                        		"published" : { "type" : "string", "index" : "not_analyzed" },
                        		"link" : { "type" : "string", "index" : "not_analyzed" },
                        		"summary" : { "type" : "string", "index" : "not_analyzed" },
                                	"timestamp" : { "type" : "string", "index" : "not_analyzed" },
					"country": { "type" : "string", "index" : "analyzed" }
                        	}
			}
                }

                aws_response = json.loads(aws.curl_put_index(ewsloader.app_config_json['index']['rss'],index_setting))
                
                if aws_response and 'acknowledged' in aws_response:
                    if str(aws_response['acknowledged']) == "True":
                        logging.info("AWS, PASS, index %s created %s" % (ewsloader.app_config_json['index']['rss'],aws_response))
                    else:
                        logging.debug("AWS, Error, unable to creaate index %s" % (ewsloader.app_config_json['index']['rss'],aws_response))
                        exit(1)
        except Exception as ve:
            logging.debug("AWS, Exception, AWSConnector Exception : %s" % str(ve))
            exit(1)

        country = Monitor(ewsloader.country_config_json[COUNTRY]['track'][0],
            ewsloader.country_config_json[COUNTRY]['log_file'])
        logging.info("Country %s: PASS, Loaded %s" % (COUNTRY,ewsloader))

        ''' TEMPLATE END '''

        # create RSS connector
        try:
            for rss_link in ewsloader.country_config_json[COUNTRY]["rss_links"]:
                service = RSSConnector(rss_link)
                try:
                        service.parse_feed()
                        feeds = service.get_feed(ewsloader.country_config_json[COUNTRY]["regex"])

                        if feeds:
                            with open("%s/%s" %("/".join(script_path[:-2]),ewsloader.app_config_json['log']['RSS_FD']), 'a') as log_fd:
                                writer = csv.DictWriter(
                                        log_fd, fieldnames=ewsloader.app_config_json['log']['RSS_FIELDS'], delimiter=',', quoting=csv.QUOTE_ALL)
                                logging.info('LOG_RSS: Started')
                                for feed in feeds:
                                        if 'summary' in feed:
                                            try:
                                                 feed['timestamp'] = time.time()
						 feed['country'] = COUNTRY
                                                 e_result = service.extract_fields(feed)
                                                 if e_result:
							    key = e_result['link'].split('/')
							    if key:
                                                            	aws_put_resp = aws.curl_put_generic(e_result,key[-1])
                                                            	logging.info("AWS, PASS, put %s rss:%s" % (aws_put_resp,key[-1]))
                                                            	writer.writerow(e_result)
                                            except Exception as e:
                                                    logging.debug("AWS, Exception, put Exception : %s" % str(e))
                except Exception as ve:
                    logging.debug("RSS, Exception, RSSConnector feed Exception : %s" % str(ve))
                    raise
        except Exception as ve:
                logging.debug("RSS, Exception, RSSConnector Exception : %s" % str(ve))
                exit(1)

    except Exception as ews:
        logging.debug("Country %s: Exception, when loading: %s" %(COUNTRY,str(ews)))
        exit(1)
