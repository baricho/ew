#!/usr/bin/python

import feedparser
import time
from subprocess import check_output
import sys

url = 'http://feeds.bbci.co.uk/news/world/rss.xml?edition=uk#'

#
# function to get the current time
#
current_time_millis = lambda: int(round(time.time() * 1000))
current_timestamp = current_time_millis()

#
# get the feed data from the url
#
feed = feedparser.parse(url)

#
# figure out which posts to print
#
posts_to_print = []
posts_to_skip = []

for post in feed.entries:
    # if post is already in the database, skip it
    # TODO check the time
    found = "%s. %s. link %s" % (post.title,post.summary,post.id)
    posts_to_print.append(found)

for r in posts_to_print:
    print r + "|" + str(current_timestamp) + "\n"
