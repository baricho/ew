#!/usr/bin/python
import sys
import os
script_path = os.path.split(os.path.realpath(__file__))[0].split('/')
sys.path.append('%s/mod/' % "/".join(script_path[:-2]))
import logging
import json
from Connector import *
from Loader import *
from EWSLoader import EWSLoader
import time
import csv

COUNTRY='ss'

if __name__ == "__main__":
    ewsloader = None
    aws = None      # placeholder for amazon elastic search connector

    ''' TEMPLATE START '''
    logging.basicConfig(level=logging.DEBUG,
        filename='/tmp/google.txt',
        format='%(asctime)s, %(message)s')
    logging.info('STARTED')
    
    try:
        ewsloader = EWSLoader('app_config.json','config.json',"/".join(script_path[:-2]))

        # create AWS connector
        try:
            aws = AWSConnector(ewsloader.app_config_json['elasticsearch'],ewsloader.country_config_json["google_indice_mapping"])
            aws_response = json.loads(aws.curl_get(ewsloader.app_config_json['index']['google']))
            if ewsloader.app_config_json['index']['google'] in aws_response:
                logging.info("AWS, PASS, index %s exists %s" % (ewsloader.app_config_json['index']['google'],aws_response))
            else:
                logging.debug("AWS, Error, index %s missing" % ewsloader.app_config_json['index']['google'])
                # create index
                index_setting = {
                        "settings" : {
                                "number_of_shards" : 1
                        }
		}
		index_setting["mappings"] = {
			ewsloader.app_config_json['index']['google'] :  {
                		"properties" : {
                        		"title" : { "type" : "string", "index" : "analyzed" },
                                	"snippet" : { "type" : "string", "index" : "not_analyzed" },
                                	"link" : { "type" : "string", "index" : "not_analyzed" },
					"cacheId" : { "type" : "string", "index" : "not_analyzed" },
					"kind" : { "type" : "string", "index" : "analyzed" },
                                	"timestamp" : { "type" : "string", "index" : "not_analyzed" },
                                	"country": { "type" : "string", "index" : "analyzed" }
                        	}
			}
                }
                aws_response = json.loads(aws.curl_put_index(ewsloader.app_config_json['index']['google'],index_setting))
                if aws_response and 'acknowledged' in aws_response:
                    if str(aws_response['acknowledged']) == "True":
                        logging.info("AWS, PASS, index %s created %s" % (ewsloader.app_config_json['index']['google'],aws_response))
                    else:
                        logging.debug("AWS, Error, unable to creaate index %s" % (ewsloader.app_config_json['index']['google'],aws_response))
                        exit(1)
        except Exception as ve:
            logging.debug("AWS, Exception, AWSConnector Exception : %s" % str(ve))
            exit(1)

        country = Monitor(ewsloader.country_config_json[COUNTRY]['track'][0],
            ewsloader.country_config_json[COUNTRY]['log_file'])
        logging.info("Country %s: PASS, Loaded %s" % (COUNTRY,ewsloader))

        ''' TEMPLATE END '''

        # create connector
        try:
            service = GoogleConnector(ewsloader.app_config_json['google']['google_api'],
                ewsloader.app_config_json['google']['google_api_v'],
                ewsloader.app_config_json['google']['developerKey'],
                ewsloader.app_config_json['google']['cx'])
            result = service.get_search("ethiopia")

	    with open("%s/%s" %("/".join(script_path[:-2]),ewsloader.app_config_json['log']['GOOGLE_FD']), 'a') as log_fd:
                writer = csv.DictWriter(
                        log_fd, fieldnames=ewsloader.app_config_json['log']['GOOGLE_FIELDS'], delimiter=',', quoting=csv.QUOTE_ALL)
                logging.info('LOG_GOOGLE: Started')
                for gs in result['items']:
                        if 'title' in gs:
                                try:
				     gs['timestamp'] = time.time()
				     gs['country'] = COUNTRY
                                     e_result = service.extract_fields(gs)
                                     if e_result:
                                                aws_put_resp = aws.curl_put_google(e_result)
                                                logging.info("AWS, PASS, put %s google:%s" % (aws_put_resp, e_result["cacheId"]))
                                                writer.writerow(e_result)
                                except Exception as e:
                                        logging.debug("AWS, Exception, put Exception : %s" % str(e))
                                        exit(1)
        except Exception as ve:
            logging.debug("GOOGLE, Exception, GoogleConnector Exception : %s" % str(ve))
            exit(1)
            
    except Exception as ews:
        logging.debug("Country %s: Exception, when loading: %s" %(COUNTRY,str(ews)))
        exit(1)


