from setuptools import setup

setup(name='ews',
      version='1.0',
      description='Early Warning System',
      url='https://gitlab.com/baricho/ew',
      author='Solomon Tadesse',
      author_email='solomon.t.b@gmail.com',
      license='MIT',
      packages=['ews'],
      install_requires=[
          'google-api-python-client',
	  'feedparser',
          'pycurl',
          'json',
          'cStringIO',
          'urllib',
          'TwitterAPI',
      ],
      zip_safe=False)
